from random import randrange
from math import tanh
import pygame, time

pygame.init()

stabsound = pygame.mixer.Sound('./pildid/sound/stab.wav')
bashsound = pygame.mixer.Sound('./pildid/sound/bash.wav')
misssound = pygame.mixer.Sound('./pildid/sound/miss.wav')
catsound = pygame.mixer.Sound('./pildid/sound/cat.wav')
dinosound = pygame.mixer.Sound('./pildid/sound/dino.wav')
elksound = pygame.mixer.Sound('./pildid/sound/elk.wav')
bearsound = pygame.mixer.Sound('./pildid/sound/bear.wav')
mammothsound = pygame.mixer.Sound('./pildid/sound/mammoth.wav')

def player_attack(skill, attacker, defender):
	try:
		if skill == 'bash':
			damage = int(bash(attacker))
		elif skill == 'stab':
			damage = int(stab(attacker))
	except:
		return 'Not enough stamina'
	hit_probability = int((0.5 + tanh((attacker.dexterity-defender.agility)/37.5)/2)*100)
	arv = randrange(0,100)
	if hit_probability >=  arv:
		defender.current_HP -= damage
		vastus = attacker.name + " did "+  str(damage) + " damage."
		if skill == 'bash':
			bashsound.play()
		if skill == 'stab':
			stabsound.play()
	else:
		vastus = attacker.name + " missed."
		misssound.play()
	return vastus
	
def monster_attack(attacker, defender):
	hit_probability = int((0.5 + tanh((attacker.dexterity-defender.agility)/37.5)/2)*100)
	arv = randrange(0,100)
	if hit_probability >=  arv:
		defender.current_HP -= attacker.strength
		vastus = attacker.name + " did "+  str(attacker.strength) + " damage."
		time.sleep(0.5)
		if attacker.name == "Cat":
			catsound.play()
		elif attacker.name == "Dino":
			dinosound.play()
		elif attacker.name == "Elk":
			elksound.play()
		elif attacker.name == "Bear":
			bearsound.play()
		elif attacker.name == "Mammoth":
			mammothsound.play()
	else:
		vastus = attacker.name + " missed."
		time.sleep(0.5)
		misssound.play()
	return vastus
		
		
def bash(attacker):
	if attacker.current_stamina >= 5:
		attacker.current_stamina = attacker.current_stamina - 5
		damage = int(attacker.strength) + attacker.skills['bash'][0] * 1.3
		attacker.skills['bash'][1] += 2
		attacker.strength += 0.05
	else:
		return 'Not enough stamina'
	return damage
	
def stab(attacker):
	
	if attacker.current_stamina >= 5:
		damage = int(attacker.dexterity) + attacker.skills['stab'][0] * 1.1
		attacker.current_stamina -= 5
		attacker.skills['stab'][1] += 2
		attacker.dexterity += 0.1
	else:
		return 'Not enough stamina'
	return damage
	
def stun(attacker, defender):
	
	if attacker.current_stamina >= 10:
		attacker.current_stamina -= 10
		attacker.skills['stun'][1] += 3
		defender.stun_count += int(attacker.skills['stun'][0] * 1.2)
		return "You have stunned the enemy"
	
	else:
		return "Not enough stamina"
