class Items(object):
	
	def __init__(self, name, required_stats, weight, damage, block_power, durability, speed_reduction, place, identity):
		self.name = name
		self.required_stats = required_stats
		self.weight = weight
		self.damage = damage
		self.block_power = block_power
		self.durability = durability
		self.speed_reduction = speed_reduction
		self.place = place
		self.identity = identity
		

club = Items('club', {'strength': 5},  20, 5, 1, 20, 0.9,'right hand','weapon')
		
