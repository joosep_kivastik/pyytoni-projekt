#autorid: Joosep Kivastik & Kaarel Adamson

import pygame, sys, time
from pygame.locals import *
import pickle, _monster, _skills, _menu
from _player import player
import random


FPS = 60
fpsClock = pygame.time.Clock()
WINDOWWIDTH = 675
WINDOWHEIGHT = 675
BOXSIZE = 45

icons = [[_monster.the_cat, _monster.the_cat.sprite_number], [_monster.the_wei, _monster.the_wei.sprite_number], [_monster.the_bear, _monster.the_bear.sprite_number], [_monster.the_elk,_monster.the_elk.sprite_number], [_monster.the_mammoth,_monster.the_mammoth.sprite_number]]
icon = icons[0][1]

world = pickle.load(open("maailm.p", "rb"))
monster_lists = _monster.monster_lists
board = []

pygame.init()
DISPLAYSURF = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT),0,32)
pygame.display.set_caption('Prehistoric man with club v0.02')
stunsound = pygame.mixer.Sound('./pildid/sound/stun.wav')
battlebk = pygame.image.load('./pildid/battle1.jpg')
namebox = pygame.image.load('./pildid/namebox.png')
itembox = pygame.image.load('./pildid/namebox.png')
skillbox = pygame.image.load('./pildid/skills.png')
itembox = pygame.image.load('./pildid/items.png')
combatlog = pygame.image.load('./pildid/log.png')
victory = pygame.image.load('./pildid/v6it.png')

def music():
	music_menu = pygame.mixer.music.load('./pildid/sound/menu.mp3')
	pygame.mixer.music.play(-1)

def left_top_coords_of_box(boxx,boxy):
	left = boxx * BOXSIZE
	top = boxy * BOXSIZE
	return (left, top)


def get_box_at_pixel(x,y):
	for boxx in range (0, WINDOWWIDTH):
		for boxy in range (0, WINDOWHEIGHT):
			left, top = left_top_coords_of_box(boxx,boxy)
			boxRect = pygame.Rect(left, top, BOXSIZE, BOXSIZE)
			if boxRect.collidepoint(x,y):
				return (boxx,boxy)
	return (None, None)


def write(text,x,y,size,color):
	font_obj = pygame.font.Font('./pildid/D3Stonism.ttf', size)
	text_surface_obj = font_obj.render(text, True, color)
	text_rect_obj = text_surface_obj.get_rect()
	text_rect_obj.center = (x,y)
	DISPLAYSURF.blit(text_surface_obj, text_rect_obj)


def generate_boxes_for_drawing(playerx,playery):
	boxx, boxy = get_box_at_pixel(playerx,playery)
	boxx += player.boxx
	boxy += player.boxy
	lisatav = []
	x = -1
	if boxx < 7:
		start_boxx = 0
		end_boxx = 15
	elif boxx > 42:
		start_boxx = 35
		end_boxx = 50
	else:
		start_boxx = boxx-7
		end_boxx = boxx + 8
	if boxy < 7:
		start_boxy = 0
		end_boxy = 15
	elif boxy > 42:
		start_boxy = 35
		end_boxy = 50
	else:
		start_boxy = boxy-7
		end_boxy = boxy+8
	for column in range(start_boxx,end_boxx):
		x += 1
		y = -1
		for row in range(start_boxy, end_boxy):
			y += 1
			lisatav.clear()
			lisatav.extend(world[column][row])
			lisatav.extend((x,y))
			board.append(lisatav.copy())
	return board

def draw_player(playerx,playery):
	DISPLAYSURF.blit(player.main_sprite,(playerx,playery))


	
def draw_monster(monster,x,y):
	DISPLAYSURF.blit(monster.main_sprite,(x,y))
	
	
def battle(monster):
	pygame.mixer.music.stop()
	pygame.mixer.music.load('./pildid/sound/fight.mp3')
	pygame.mixer.music.play()
	DISPLAYSURF.blit(battlebk, (0,0))
	DISPLAYSURF.blit(player.battle_sprite, (105,270))
	DISPLAYSURF.blit(monster.battle_sprite,(375,150))
	askillbox=1
	log=[]
	while True:
		dead = False
		mousex, mousey = pygame.mouse.get_pos()
		DISPLAYSURF.blit(namebox,(25,55))
		DISPLAYSURF.blit(namebox,(350,55))
		DISPLAYSURF.blit(combatlog, (3,425))
		write('Combat log',100,475,20,(64,64,64))
		
		write(monster.name,500,90,18,(64,64,64))
		

		for event in pygame.event.get():
			
			
			if event.type == KEYUP and event.key == K_ESCAPE:
				pygame.quit()
				sys.exit()

			elif askillbox==1:
				DISPLAYSURF.blit(skillbox,(320,430))
				if 385 < mousex < 483 and  533 < mousey < 563:
					write('Bash',435,550,35,(245,255,56))
					write('Stun',435,600,35,(64,64,64))
					write('Skills',420,485,30,(64,64,64))
					write('Items',580,480,30,(64,64,64))
					write('Stab',560,550,35,(64,64,64))
					write('Rest',560,603,35,(64,64,64))
					if event.type == MOUSEBUTTONUP:
						log.insert(0,(_skills.player_attack('bash',player, monster)))
						
						if monster.stun_count == 0:
							log.insert(0,(_skills.monster_attack(monster,player)))
							
						else:
							log.insert(0,(monster.name + ' is stunned.'))
							monster.stun_count = monster.stun_count - 1

				elif 385 < mousex < 483 and  585 < mousey < 617:
					write('Stun',435,600,35,(255,245,56))
					write('Bash',435,550,35,(64,64,64))
					write('Skills',420,485,30,(64,64,64))
					write('Items',580,480,30,(64,64,64))
					write('Stab',560,550,35,(64,64,64))
					write('Rest',560,603,35,(64,64,64))
					if event.type == MOUSEBUTTONUP:
						if _skills.stun(player,monster) == "Not enough stamina":
							log.insert(0,('Not enough stamina'))
							log.insert(0,(_skills.monster_attack(monster,player)))
						else:
							log.insert(0,('You have stunned the enemy'))
							stunsound.play()
							log.insert(0,(monster.name + ' is stunned'))

				elif 515 < mousex < 605 and  533 < mousey < 563:
					write('Stab',560,550,35,(255,245,56))
					write('Bash',435,550,35,(64,64,64))
					write('Stun',435,600,35,(64,64,64))
					write('Skills',420,485,30,(64,64,64))
					write('Items',580,480,30,(64,64,64))
					write('Rest',560,603,35,(64,64,64))
					if event.type == MOUSEBUTTONUP:
						log.insert(0,(_skills.player_attack('stab',player, monster)))
						if monster.stun_count == 0:
							log.insert(0,(_skills.monster_attack(monster,player)))
						else:
							log.insert(0,(monster.name + ' is stunned'))
							monster.stun_count = monster.stun_count - 1
						  
				elif 530 < mousex < 630 and  460 < mousey < 500:
					write('Bash',435,550,35,(64,64,64))
					write('Stun',435,600,35,(64,64,64))
					write('Skills',420,485,30,(64,64,64))
					write('Stab',560,550,35,(64,64,64))
					write('Items',580,480,30,(255,245,56))
					write('Rest',560,603,35,(64,64,64))
					if event.type == MOUSEBUTTONUP:
						askillbox=0
						DISPLAYSURF.blit(itembox,(320,430))
						write('Skills',415,481,30,(64,64,64))
						write('Items',570,485,30,(64,64,64))
						
				elif 515 < mousex < 605 and  585 < mousey < 617 and player.current_stamina <= 145:
					write('Bash',435,550,35,(64,64,64))
					write('Stun',435,600,35,(64,64,64))
					write('Skills',420,485,30,(64,64,64))
					write('Items',580,480,30,(64,64,64))
					write('Stab',560,550,35,(64,64,64))
					write('Rest',560,603,35,(255,245,56))
					if event.type == MOUSEBUTTONUP:           
						if player.current_stamina + 10 >= player.max_stamina:
							player.current_stamina = player.max_stamina
							log.insert(0, ("Your stamina is now full"))
						else:
							player.current_stamina = player.current_stamina + 10
							log.insert(0, ("You gained 10 stamina!"))
						if monster.stun_count == 0:
							log.insert(0,(_skills.monster_attack(monster,player)))
						else:
							log.insert(0,(monster.name + ' is stunned'))
							monster.stun_count = monster.stun_count - 1
										
				else:
					write('Bash',435,550,35,(64,64,64))
					write('Stun',435,600,35,(64,64,64))
					write('Skills',420,485,30,(64,64,64))
					write('Items',580,480,30,(64,64,64))
					write('Stab',560,550,35,(64,64,64))
					write('Rest',560,603,35,(64,64,64))
					
			elif askillbox == 0:
				DISPLAYSURF.blit(itembox,(320,430))
				if 365 < mousex < 475 and  465 < mousey < 495:
					write('Items',570,485,30,(64,64,64))
					write('Skills',415,481,30,(255,245,56))
					if event.type == MOUSEBUTTONUP:
						askillbox=1
						DISPLAYSURF.blit(skillbox,(320,430))
						write('Bash',435,550,35,(64,64,64))
						write('Stun',435,600,35,(64,64,64))
						write('Skills',420,485,30,(64,64,64))
						write('Items',580,480,30,(64,64,64))
						write('Stab',560,550,35,(64,64,64))
						write('Rest',560,603,35,(64,64,64))
				else:
					write('Skills',415,481,30,(64,64,64))
					write('Items',570,485,30,(64,64,64))
				
			if player.current_HP <= 0:
				player.current_HP = 0
				time.sleep(2)
				g=open('highscores.txt', 'a')
				g.write(player.name+","+str(player.score)+"\n")
				g.close()
				DISPLAYSURF.fill((0,0,0))
				write('You are dead',WINDOWWIDTH/2,WINDOWHEIGHT/2,50,(255,0,0))
				write('Score: ' + str(player.score), WINDOWWIDTH/2, WINDOWHEIGHT/2 + 50, 50, (255,0,0))
				pygame.display.update()
				time.sleep(3)
				pygame.quit()	
				sys.exit()

			try:
				write(log[0],170,520,18,(64,64,64))
				write(log[1],170,543,18,(64,64,64))
				write(log[2],170,578,12,(64,64,64))
				write(log[3],170,590,12,(64,64,64))
				write(log[4],170,602,12,(64,64,64))
				write(log[5],170,614,12,(64,64,64))
				
			except:
				False

			write('HP: ' + str(monster.current_HP) + '/' + str(monster.max_HP),500,115,18,(64,64,64))
			write('HP: ' + str(player.current_HP) + '/' + str(player.max_HP), 175, 90, 18, (64,64,64))
			write('Stamina: ' + str(player.current_stamina) + '/' + str(player.max_stamina),175,115,18,(64,64,64))

			pygame.display.update()
						
			if monster.current_HP <= 0:
				world[player.boxx+7][player.boxy+7][3] = False
				monster.state = False
				
		if monster.state == False:
			player.score += monster.points
			player.check_to_upgrade_skills()
			DISPLAYSURF.blit(victory, (85,95))
			write('You defeated '+monster.name+'!', 340,200,35,(64,64,64))
			write('Bash     (Level '+ str(player.skills['bash'][0])+ ')', 340,282,32,(64,64,64))
			write('Experience:'+ str(player.skills['bash'][1])+'/'+ str(player.skills['bash'][0]*10), 340,320,25,(64,64,64))
			write('Stab     (Level '+ str(player.skills['stab'][0])+ ')', 340,372,32,(64,64,64))
			write('Experience:'+ str(player.skills['stab'][1])+'/'+ str(player.skills['stab'][0]*10), 340,410,25,(64,64,64))
			write('Stun     (Level '+ str(player.skills['stun'][0])+ ')', 340,462,32,(64,64,64))
			write('Experience:'+ str(player.skills['stun'][1])+'/'+ str(player.skills['stun'][0]*10), 340,500,25,(64,64,64))
			pygame.display.update()
			time.sleep(2)
			pygame.mixer.music.stop()
			pygame.mixer.music.load('./pildid/sound/menu.mp3')
			pygame.mixer.music.play(-1)
			break					
	

def draw_world(listike,playerx,playery):
	for boxes in listike:
		left, top = left_top_coords_of_box(boxes[5],boxes[6])
		pygame.draw.rect(DISPLAYSURF,boxes[2],(left,top,BOXSIZE,BOXSIZE),0)
		if boxes[3] == True:
			pic = icons[boxes[4]][0].main_sprite
			DISPLAYSURF.blit(pic,(left, top))


			
def move_monsters(monster_lists,world):
	for monster_list in monster_lists:
		for monster in monster_list:
			if monster.state == True and time.time() - monster.last_moved >= 1/monster.speed:
				monster.move(world)
				monster.last_moved = time.time()
				
def end_of_level():
	a = 0
	b = 0
	for monster_list in monster_lists:
		for monster in monster_list:
			b += 1
			if monster.state == False:
				a += 1
	
	if a > int(b/2):
		return True
	else:
		return False
		
def level_monsters(monster_lists):
	for monster_list in monster_lists:
		for monster in monster_list:
			monster.strength += monster.level
			monster.dexterity += monster.level
			monster.agility += monster.level
			monster.max_HP += 10*monster.level
			monster.current_HP = monster.max_HP
			monster.level += 1
			monster.state = True
			
def restore_world(monster_lists, world,player):
	player.current_HP = player.max_HP
	player.current_stamina = player.max_stamina
	for monster_list in monster_lists:
		for monster in monster_list:
			monster.move_to_start(world)

			
	
newlevel = pygame.image.load('./pildid/newlevel.png')
scoreboard = pygame.image.load('./pildid/boxinthecorner.png')
music()

suund = ""
player.name, sprait, b_sprite =  _menu.start()
player.main_sprite = pygame.image.load(sprait)
player.battle_sprite = pygame.image.load(b_sprite)
level=1
stall = False
while True:
	if end_of_level():
		if stall == True:		
			restore_world(monster_lists, world,player)
			level_monsters(monster_lists)
			player.boxx = 0
			player.boxy = 0
			player.x = 315
			player.y = 315
			stall = False
			level += 1
			DISPLAYSURF.blit(newlevel, (120,180))
			pygame.display.update()
			time.sleep(5)
		else:
			stall = True
		
	board = generate_boxes_for_drawing(player.x,player.y)
	draw_world(board,player.x,player.y)
	draw_player(player.x,player.y)
	DISPLAYSURF.blit(scoreboard, (5,522))
	write("Score: "+str(player.score), 130,570,25, (64,64,64))
	write("Level: "+str(level), 130,615,25, (64,64,64))
	
	
	for event in pygame.event.get():
		if event.type == QUIT or (event.type == KEYUP and event.key == K_ESCAPE):
			pygame.quit()
			sys.exit()
			
		elif event.type == KEYUP and event.key == K_m:
			move_monsters(monster_lists,world)
			
		elif event.type == KEYUP and event.key == K_w:
			suund = 'w'

		elif event.type == KEYUP and event.key == K_s:
			suund = 's'


		elif event.type == KEYUP and event.key == K_d:
			suund = 'd'

		elif event.type == KEYUP and event.key == K_a:
			suund ='a'

		if suund != "":
			player.move(suund,world)
			suund = ""
		
	move_monsters(monster_lists,world)			
	if world[player.boxx+7][player.boxy+7][3] == True:
			for monster_list in monster_lists:
				for monster in monster_list:
					if monster.boxx == player.boxx+7 and monster.boxy == player.boxy+7:
						battle(monster)
	
	
	board.clear()
	pygame.display.update()
	fpsClock.tick(FPS)

