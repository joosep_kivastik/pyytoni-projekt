import random, pygame
import _items, _player
import pickle
import time

class Monster(object):
		
	def __init__(self):
		self.sprite_number = 0
		self.algne_boxx = 0
		self.algne_boxy = 0
		self.boxx = 0
		self.boxy = 0
		
	def move_random(self,world):

		while True:
			suundx = random.choice((-1,1,0))
			suundy = random.choice((-1,1,0))
			if 0 <= self.boxx + suundx <= 49 and 0 <= self.boxy + suundy <= 49 :
				if world[self.boxx + suundx][self.boxy + suundy][2] != (0,0,255) and world[self.boxx + suundx][self.boxy + suundy][3] != True:
					world[self.boxx][self.boxy][3] = False
					world[self.boxx+suundx][self.boxy+suundy][3] = True
					world[self.boxx+suundx][self.boxy+suundy][4] = self.sprite_number
					self.boxx += suundx
					self.boxy += suundy
					break
				
	def move(self,world):
		yldine_suundx = _player.player.boxx + 7 - self.boxx
		yldine_suundy = _player.player.boxy + 7 - self.boxy
		if int((yldine_suundx ** 2 + yldine_suundy ** 2) ** 0.5) <= self.radius:
			if yldine_suundx < 0:
				suundx = -1
			elif yldine_suundy > 0:
				suundx = 1
			else:
				suundx = 0
				
			if yldine_suundy < 0:
				suundy = -1
			elif yldine_suundy > 0:
				suundy = 1
			else:
				suundy = 0
				
			if world[self.boxx + suundx][self.boxy + suundy][2] != (0,0,255) and world[self.boxx + suundx][self.boxy + suundy][3] != True:
				world[self.boxx][self.boxy][3] = False
				world[self.boxx+suundx][self.boxy+suundy][3] = True
				world[self.boxx+suundx][self.boxy+suundy][4] = self.sprite_number
				self.boxx += suundx
				self.boxy += suundy
			
			else:
				self.move_random(world)
		
		else:
			self.move_random(world)
			
	def move_to_start(self,world):
		world[self.boxx][self.boxy][3] = False
		world[self.algne_boxx][self.algne_boxy][3] = True
		world[self.algne_boxx][self.algne_boxy][4] = self.sprite_number
		self.boxx = self.algne_boxx
		self.boxy = self.algne_boxy
			
	
	
class Cat(Monster):
	def __init__(self):
		self.name = 'Cat'
		self.main_sprite = pygame.image.load('./pildid/cat_main.png')
		self.map_sprite = pygame.image.load('./pildid/cat_map.png')
		self.battle_sprite = pygame.image.load('./pildid/cat_battle.png')
		self.sprite_number = 0
		self.strength = 3
		self.dexterity = 15
		self.agility = 5
		self.max_HP = 20
		self.current_HP = self.max_HP
		self.boxx = 0
		self.boxy = 0
		self.algne_boxx = 0
		self.algne_boxy = 0
		self.radius = 10
		self.state = True
		self.speed = 5
		self.last_moved = 0
		self.stun_count = 0
		self.level = 1
		self.points = 5

class Elk(Monster):
	def __init__(self):
		self.name = 'Elk'
		self.main_sprite = pygame.image.load('./pildid/elk_main.png')
		self.map_sprite = pygame.image.load('./pildid/elk_map.png')
		self.battle_sprite = pygame.image.load('./pildid/elk_battle.png')
		self.sprite_number = 3
		self.strength = 6
		self.dexterity = 7
		self.agility = 6
		self.max_HP = 50
		self.current_HP = self.max_HP
		self.boxx = 0
		self.boxy = 0
		self.algne_boxx = 0
		self.algne_boxy = 0
		self.radius = 6
		self.state = True
		self.speed = 3
		self.last_moved = 0
		self.stun_count = 0
		self.level = 1
		self.points = 7

class Bear(Monster):
	def __init__(self):
		self.name = 'Bear'
		self.main_sprite = pygame.image.load('./pildid/bear_main.png')
		self.map_sprite = pygame.image.load('./pildid/bear_map.png')
		self.battle_sprite = pygame.image.load('./pildid/bear_battle.png')
		self.sprite_number = 2
		self.strength = 10
		self.dexterity = 7
		self.agility = 5
		self.max_HP = 100
		self.current_HP = self.max_HP
		self.boxx = 0
		self.boxy = 0
		self.algne_boxx = 0
		self.algne_boxy = 0
		self.radius = 5
		self.state = True
		self.speed = 2
		self.last_moved = 0
		self.stun_count = 0
		self.level = 1
		self.points = 7

class Mammoth(Monster):
	def __init__(self):
		self.name = 'Mammoth'
		self.main_sprite = pygame.image.load('./pildid/mamm_main.png')
		self.map_sprite = pygame.image.load('./pildid/mamm_map.png')
		self.battle_sprite = pygame.image.load('./pildid/mamm_battle.png')
		self.sprite_number = 4
		self.strength = 15
		self.dexterity = 8
		self.agility = 2
		self.max_HP = 200
		self.current_HP = self.max_HP
		self.boxx = 0
		self.boxy = 0
		self.algne_boxx = 0
		self.algne_boxy = 0
		self.radius = 10
		self.state = True
		self.speed = 1
		self.last_moved = 0
		self.stun_count = 0
		self.level = 1
		self.points = 10

		
class Wei(Monster):
	def __init__(self):
		self.name = 'Wei'
		self.main_sprite = pygame.image.load('./pildid/dino_main.png')
		self.map_sprite = pygame.image.load('./pildid/dino_map.png')
		self.battle_sprite = pygame.image.load('./pildid/dino_battle.png')
		self.sprite_number = 1
		self.strength = 7
		self.dexterity = 7
		self.agility = 7
		self.max_HP = 50
		self.current_HP = self.max_HP
		self.boxx = 0
		self.boxy = 0
		self.algne_boxx = 0
		self.algne_boxy = 0
		self.radius = 4
		self.state = True
		self.speed = 3
		self.last_moved = 0
		self.stun_count = 0
		self.level = 1
		self.points = 9


cats = []
weis = []
bears = []
elks = []
mammoths = []
try:
	catboxs, weiboxs, elkboxs, mammothboxs, bearboxs = pickle.load(open("monsters.p","rb"))
	for cat in catboxs:
		cats.append(Cat())
		cats[-1].boxx = cat[0]
		cats[-1].boxy = cat[1]
		cats[-1].algne_boxx = cats[-1].boxx
		cats[-1].algne_boxy = cats[-1].boxy
		cats[-1].last_moved = time.time()
	for wei in weiboxs:
		weis.append(Wei())
		weis[-1].boxx = wei[0]
		weis[-1].boxy = wei[1]
		weis[-1].algne_boxx = weis[-1].boxx
		weis[-1].algne_boxy = weis[-1].boxy
		weis[-1].last_moved = time.time()
	for bear in bearboxs:
		bears.append(Bear())
		bears[-1].boxx = bear[0]
		bears[-1].boxy = bear[1]
		bears[-1].algne_boxx = bears[-1].boxx
		bears[-1].algne_boxy = bears[-1].boxy
		bears[-1].last_moved = time.time()
	for elk in elkboxs:
		elks.append(Elk())
		elks[-1].boxx = elk[0]
		elks[-1].boxy = elk[1]
		elks[-1].algne_boxx = elks[-1].boxx
		elks[-1].algne_boxy = elks[-1].boxy
		elks[-1].last_moved = time.time()
	for mammoth in mammothboxs:
		mammoths.append(Mammoth())
		mammoths[-1].boxx = mammoth[0]
		mammoths[-1].boxy = mammoth[1]
		mammoths[-1].algne_boxx = mammoths[-1].boxx
		mammoths[-1].algne_boxy = mammoths[-1].boxy
		mammoths[-1].last_moved = time.time()
	
except:
	catboxs = []
	mammothboxs = []
	bearboxs = []
	elkboxs = []
	weiboxs = []


monster_lists = [cats, weis, bears, elks, mammoths]
the_cat = Cat()
the_wei = Wei()
the_bear = Bear()
the_elk = Elk()
the_mammoth = Mammoth()
monster_directory ={
'Cat': [catboxs],
'Wei': [weiboxs],
'Mammoth': [mammothboxs],
'Elk': [elkboxs],
'Bear': [bearboxs]
}
