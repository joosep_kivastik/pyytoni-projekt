#world_creator.alpha
#autor Joosep Kivastik


import pygame, sys
from pygame.locals import *
import pickle
import _monster
WINDOWWIDTH = 800
WINDOWHEIGHT = 800
BOXSIZE = 16

GREEN = (0,255,0)
BLUE = (0,0,255)
WHITE = (255,255,255)
HIGHLIGHTCOLOR = (0,0,255)
BLACK = (0,0,0)
HIGHLIGHTCOLOR = (255,0,0)
icons = [[_monster.the_cat, _monster.the_cat.sprite_number], [_monster.the_wei, _monster.the_wei.sprite_number], [_monster.the_bear, _monster.the_bear.sprite_number], [_monster.the_elk,_monster.the_elk.sprite_number], [_monster.the_mammoth,_monster.the_mammoth.sprite_number]]
icon = icons[0][1]



def left_top_coords_of_box(boxx, boxy):
	#convert board  coordinates to pixel coordinates
	left = boxx * BOXSIZE
	top = boxy * BOXSIZE
	return (left, top)

def get_box_at_pixel(x,y):
	for boxx in range (0, WINDOWWIDTH):
		for boxy in range (0, WINDOWHEIGHT):
			left, top = left_top_coords_of_box(boxx,boxy)
			boxRect = pygame.Rect(left, top, BOXSIZE, BOXSIZE)
			if boxRect.collidepoint(x,y):
				return (boxx,boxy)
	return (None, None)

def generate_boxes():
	#loob kastid mida vÃ¤rvida
	board = []
	for i in range(50):
		column=[]
		for y in range(50):
			column.append([i,y,BLACK,False,0])
		board.append(column)
	return board

	

def draw_highlight_box(boxx, boxy):
	#mÃ¤rgib, mis kastile joonistad
	left, top = left_top_coords_of_box(boxx, boxy)
	pygame.draw.rect(DISPLAYSURF, HIGHLIGHTCOLOR, (left, top, BOXSIZE, BOXSIZE), 4)

def draw_colored_box(color,boxx,boxy):
	#vÃ¤rvib valitud kasti
	global boxes
	boxes[boxx][boxy][2] = color

def blit_icon(obj,boxx,boxy):
	#blitz a sprite
	global boxes, icons
	_monster.monster_directory[obj.name][0].append([boxx,boxy])
	boxes[boxx][boxy][3] = True
	boxes[boxx][boxy][4] = obj.sprite_number
	obj = icons[obj.sprite_number][0].map_sprite
	left, top = left_top_coords_of_box(boxx,boxy)
	DISPLAYSURF.blit(obj,(left,top))

def pre_blit_icon(pic,boxx,boxy):
	#shows the sprite before blitting
	obj = icons[pic][0]
	left, top = left_top_coords_of_box(boxx,boxy)
	DISPLAYSURF.blit(obj.map_sprite,(left,top))

def remove_icon(boxx,boxy):
	#deletes the icon at selected box
	boxes[boxx][boxy][3] = False

def change_state(val):
	if val == True:
		return False, False, False
	else:
		return True, False, False

def change_icon(number,pic):
	global icons
	if pic == 0 and number == -1 or pic == len(icons)-1 and number == 1:
		return pic
	else:
		pic = icons[pic+number][1]
		return pic

def draw_world():
	#joonistab stabiilselt maailma
	global boxes, icons
	for column in boxes:
		for row in column:
			left, top = left_top_coords_of_box(row[0],row[1])
			pygame.draw.rect(DISPLAYSURF,row[2],(left,top,BOXSIZE,BOXSIZE),0)
			if row[3] == True:
				obj = icons[row[4]][0]
				DISPLAYSURF.blit(obj.map_sprite,(left, top))

		
	
try:
	boxes = pickle.load(open("maailm.p","rb"))
except:
	boxes = generate_boxes()
	
	

		



pygame.init()
DISPLAYSURF = pygame.display.set_mode((WINDOWWIDTH,WINDOWHEIGHT),0,32)
mousex = 0
mousey = 0
pygame.display.set_caption('Map Creator')
color = BLACK
draw_state = False
blit_state = False
remove_state = False

print ("world creator.alpha")
print("Maailmate loomiseks,kustutamine: e sinine: b, roheline: g, valge: w ja salvestamine: s")
while True:
	mouse_clicked = False
	draw_world()
	for event in pygame.event.get():
		if event.type == QUIT or (event.type == KEYUP and event.key == K_ESCAPE):
			pygame.quit()
			sys.exit()

		elif event.type == MOUSEMOTION:
			mousex, mousey = event.pos

		elif event.type == MOUSEBUTTONUP:
			mousex, mousey = event.pos
			mouse_clicked = True
			
		elif event.type == KEYUP and event.key == K_g:
			color = GREEN

		elif event.type == KEYUP and event.key == K_b:
			color = BLUE

		elif event.type == KEYUP and event.key == K_w:
			color = WHITE

		elif event.type == KEYUP and event.key == K_e:
			color = BLACK

		elif event.type == KEYUP and event.key == K_p:
			blit_state, draw_state, remove_state = change_state(blit_state)

		elif event.type == KEYUP and event.key == K_d:
			draw_state, blit_state, remove_state = change_state(draw_state)
		
		elif event.type == KEYUP and event.key == K_r:
			remove_state, draw_state, blit_state = change_state(remove_state)

		elif event.type == KEYUP and event.key == K_LEFT:
			icon = change_icon(-1,icon)

		elif event.type == KEYUP and event.key == K_RIGHT:
			icon = change_icon(1,icon)

		elif event.type == KEYUP and event.key == K_s:
			pickle.dump(boxes, open("maailm.p","wb"))
			pickle.dump([_monster.catboxs, _monster.weiboxs, _monster.bearboxs, _monster.elkboxs, _monster.mammothboxs], open("monsters.p","wb"))



	boxx, boxy = get_box_at_pixel(mousex,mousey)
	draw_highlight_box(boxx,boxy)

	if draw_state == True:
		draw_colored_box(color,boxx,boxy)

	elif blit_state == True and mouse_clicked == False:
		pre_blit_icon(icons[icon][1],boxx,boxy)

	elif blit_state == True and mouse_clicked == True:
		blit_icon(icons[icon][0],boxx,boxy)

	elif remove_state == True and mouse_clicked == True:
		remove_icon(boxx,boxy)
			

	pygame.display.update()
