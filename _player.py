import random, pygame
import _items, _skills

WINDOWWIDTH = 675
WINDOWHEIGHT = 675
BOXSIZE = 45

class Player(object):
	def add_to_stats(self):
		self.stats['strength'] = self.strength
		self.stats['dexterity'] = self.dexterity
		self.stats['agility'] = self.agility
		self.stats['HP'] = self.max_HP
		self.stats['stamina'] = self.max_stamina
		
	def __init__(self, name, strength, dexterity, agility, map_sprite, main_sprite, sprite_number, inventory, skillid, battle_sprite):
		self.name      = name
		self.strength  =  strength
		self.dexterity = dexterity
		self.agility   =   agility
		self.inventory = inventory
		self.map_sprite = map_sprite
		self.main_sprite = main_sprite
		self.battle_sprite = battle_sprite
		self.sprite_number = sprite_number
		self.max_HP = 0
		self.max_stamina = 0
		self.increase_HP_stamina()
		self.current_HP = self.max_HP
		self.current_stamina = self.max_stamina
		self.max_weight = self.strength * 1.5
		self.carrying_weight = 0
		self.score = 0
		self.boxx = 0
		self.boxy = 0
		self.x = 315
		self.y = 315
		self.equipped = {}
		self.stats = {}
		self.add_to_stats()
		self.skills = {}
		self.add_to_skills(skillid)
		
		
	def add_to_skills(self,skillid):
		for skill in skillid:
			self.skills[skill] = [1,0]
	
	def increase_HP_stamina(self):
		self.max_HP = int(self.strength * 11)
		self.max_stamina = int((self.dexterity + self.agility) * 5)
			
	def check_to_upgrade_skills(self):
		self.increase_HP_stamina()
		req_skill_exp = {1:10,2:20,3:30,4:40,5:50,6:60,7:70,8:80,9:90}
		for skill in self.skills:
			if self.skills[skill][0] in req_skill_exp:
				if self.skills[skill][1] >= req_skill_exp[self.skills[skill][0]]:
					self.skills[skill][1] =  self.skills[skill][1] - req_skill_exp[self.skills[skill][0]]
					self.skills[skill][0] += 1
					
			
		
	def drop_from_inventory(self):
		key = random.choice(list(self.inventory.keys()))
		self.inventory[key] -= 1
		if self.inventory[key] == 0:
			del self.inventory[key]
		return key

		
	def add_to_inventory(self, item):
		if item in self.inventory:
			self.inventory[item] += 1
		else:
			self.inventory[item] = 1
			
	def check_stats(self, item):
		for key in item.required_stats:
			if self.stats[key] >= item.required_stats[key]:
				return True
			else:
				return False
	
	def unequip_item(self,item):
		self.equipped[item.place] = 'Nothing'
		self.add_to_inventory(item)
	
	def check_weight(self, item):
		if item.weight + self.carrying_weight > self.max_weight:
			return False
		else:
			return True
			
	def equip_item(self, item):
		if self.check_stats(item) == True:
			if self.check_weight(item) == True:
				self.unequip_item(item)
				self.equipped[item.place] = item.name
				return item.name + ' equipped.'
			else:
				return "You cannot carry that much weight"
		else:
			return "You don't have the  required stats"
			
	def eat(self,item):
		if item.identity == 'food':
			if self.current_HP + item.effect < item.max_HP:
				self.current_HP += item.effect
			else:
				self.current_HP = self.max_HP
			return 'You ate ' + item.name
		
		else:
			return 'Who\'d want to eat that'
		
	def move(self, suund,world):
		if suund == 'w':
			if self.boxy != -7 and world[self.boxx+7][self.boxy+6][2] != (0,0,255):
				self.boxy -= 1
			if (self.boxy <= -1 and self.y != 0) or (self.y != 315 and self.y != 0):
				self.y -= BOXSIZE

		if suund == 's':
			if self.boxy != 42 and world[self.boxx+7][self.boxy+8][2] != (0,0,255):
				self.boxy += 1
			if (self.boxy >= 36 and self.y != (WINDOWHEIGHT - BOXSIZE)) or (self.y != 315 and self.y != (WINDOWHEIGHT - BOXSIZE)):
				self.y += BOXSIZE


		if suund == 'd':
			if self.boxx != 42 and world[self.boxx+8][self.boxy+7][2] != (0,0,255):
				self.boxx += 1
			if (self.boxx >= 36 and self.x != (WINDOWWIDTH - BOXSIZE)) or (self.x != 315 and self.x != (WINDOWWIDTH - BOXSIZE)):
				self.x += BOXSIZE	


		if suund == 'a':
			if self.boxx != -7 and world[self.boxx+6][self.boxy+7][2] != (0,0,255):			
				self.boxx -= 1
			if (self.boxx <= -1 and self.x != 0) or (self.x != 315 and self.x != 0):
				self.x -= BOXSIZE
				
player = Player('Player', 12,12,12,pygame.image.load('./pildid/player_mang.png'),pygame.image.load('./pildid/player_mang.png'),1,{}, {'bash','stab','stun'},pygame.image.load('./pildid/p1.png'))
