import pygame, sys, os, time
from pygame.locals import *
from random import randint

pygame.mixer.pre_init(44100, -16, 1, 256)
pygame.init()

taust = pygame.image.load('pildid/menyy.png')
parem = pygame.image.load('pildid/parem.png')
vasak = pygame.image.load('pildid/vasak.png')
p1 = pygame.image.load('pildid/p1.png')
p2 = pygame.image.load('pildid/p2.png')
p3 = pygame.image.load('pildid/p3.png')
p4 = pygame.image.load('pildid/p4.png')
choose = pygame.image.load('pildid/choose.png')
play = pygame.image.load('pildid/play.png')
playo = pygame.image.load('pildid/playo.png')
algus = pygame.image.load('pildid/start.png')
infopilt = pygame.image.load('pildid/info.png')
v2lju = pygame.image.load('pildid/exit.png')
starto = pygame.image.load('pildid/starto.png')
infoo = pygame.image.load('pildid/infoo.png')
v2ljuo = pygame.image.load('pildid/exito.png')
box = pygame.image.load('pildid/box.png')
back = pygame.image.load('pildid/back.png')
enter = pygame.image.load('pildid/enter.png')
high = pygame.image.load('pildid/high.png')
higho = pygame.image.load('pildid/higho.png')
cave = pygame.image.load('pildid/cave.png')
forget = pygame.image.load('pildid/forget.png')
info1 = pygame.image.load('pildid/info1.png')
info2 = pygame.image.load('pildid/info2.png')
info3 = pygame.image.load('pildid/info3.png')
skoorpealkiri = pygame.image.load('pildid/skoorpealkiri.png')
select1 = pygame.mixer.Sound('./pildid/sound/stones1.wav')
select2 = pygame.mixer.Sound('./pildid/sound/stones2.wav')
select3 = pygame.mixer.Sound('./pildid/sound/stones3.wav')
select4 = pygame.mixer.Sound('./pildid/sound/stones4.wav')
select5 = pygame.mixer.Sound('./pildid/sound/stones5.wav')
select = [select1, select2, select3, select4, select5]

def readscores():
	f=open('highscores.txt')
	rida = f.readline()
	nimed=[]
	punktid=[]
	while rida is not "":
		rida=rida.split(",")
		nimed+=[rida[0]]
		punktid+=[int(rida[1].rstrip())]
		rida = f.readline()
	return nimed, punktid

def gethighest(skoorid,kasutajad):
	skoor=max(skoorid)
	print(skoor)
	koht=skoorid.index(skoor)
	skoor=skoorid.pop(koht)
	nimi=kasutajad.pop(koht)
	return skoorid, kasutajad, skoor, nimi




def getrect(item, nr, x, y):
	item=item.get_rect()
	item.x, item.y = x[nr],y[nr]
	return item

def get_key():
	while 1:
		global event
		event = pygame.event.poll()
		if event.type == KEYDOWN:
			select[randint(1,5)-1].play()
			return 2
		elif event.type == MOUSEBUTTONDOWN:
			select[randint(1,5)-1].play()
			return 1
		elif event.type == MOUSEMOTION:
			return 3
		else:
			pass

def get_key1():
	while 1:
		global event
		event = pygame.event.poll()
		if event.type == KEYUP:
			return event.key
		else:
			pass
	
	
def joonista(aken, sisend):
	font=pygame.font.Font("pildid/D3Stonism.ttf",20)
	nimetekst=font.render((sisend), 1,(105,105,105))
	recttekst = nimetekst.get_rect()
	recttekst.center = (345, 495)
	aken.blit(nimetekst, recttekst)

def ask(current_string):
	pygame.font.init()
	inkey=get_key1()
	try:
		if inkey == K_BACKSPACE:
			current_string.pop()
		elif inkey == K_MINUS:
			current_string.append("_")
		elif inkey <= 127 and len(current_string)<8:
			current_string.append(chr(inkey))
	except:
		current_string = current_string
	nimi = "".join(current_string)
	nimi = nimi.upper()
	return nimi


def start():
	nupud = [algus, infopilt, v2lju, high]
	koordx = [255, 275, 280, 155]
	koordy = [170, 275, 490, 385]
	nupudo = [starto, infoo, v2ljuo, higho]
	aken = pygame.display.set_mode((675,675))
	aken.fill( (192,192,192) )
	aken.blit(taust, (85,110))
	aken.blit(cave, (45,10))
	global event
	event = pygame.event.poll()
	while True:
		loendur=0
		for nupp in nupud:
			rnupp=getrect(nupp, loendur, koordx, koordy)
			if rnupp.collidepoint(pygame.mouse.get_pos()):
				aken.blit(nupudo[loendur], (koordx[loendur],koordy[loendur]))
				if event.type == pygame.MOUSEBUTTONDOWN:
					select[randint(1,5)-1].play()
					if loendur == 0:
						return character()
					elif loendur == 1:
						return info()
					elif loendur == 2:
						pygame.display.quit()
						sys.exit()
					elif loendur == 3:
						return highscores()
			else:
				aken.blit(nupud[loendur], (koordx[loendur],koordy[loendur]))
			loendur += 1
		pygame.display.flip()
		aken.blit(taust, (85,110))
		event = pygame.event.poll()
		


def character():
	spraidid=[p1,p2,p3,p4]
	nupud = [parem, vasak, play, back]
	koordx = [420, 220, 273, 120]
	koordy = [270, 270, 550, 160]
	nupudo = [parem, vasak, playo, back]
	sprait = 0
	aken = pygame.display.set_mode((675,675))
	aken.fill( (192,192,192) )

	aken.blit(taust, (85,110))
	aken.blit(choose, (235,135))
	aken.blit(enter, (195,400))
	aken.blit(cave, (45,10))
	loendur = 0
	for nupp in nupud:
		aken.blit(nupud[loendur], (koordx[loendur],koordy[loendur]))
		loendur +=1
	aken.blit(spraidid[sprait], (260,225))
	aken.blit(box, (205,460))
	
	nimi = ""
	pygame.display.flip()
	pygame.display.set_caption('Character creation')
	vastus = []
	while True:
		loendur=0
		kysimus = get_key()
		
		if kysimus == 1:
			for nupp in nupud:
				rnupp=getrect(nupp, loendur, koordx, koordy)
				if rnupp.collidepoint(pygame.mouse.get_pos()):
					aken.blit(nupudo[loendur], (koordx[loendur],koordy[loendur]))
					if loendur == 1:
						if sprait == 0:
							sprait = 3
						else:
							sprait -=1
					elif loendur == 0:
						if sprait == 3:
							sprait = 0
						else:
							sprait +=1
					elif loendur == 2:
						if nimi == "":
							aken.blit(forget, (183,235))
							pygame.display.flip()
							time.sleep(1)
							aken.blit(taust, (85,110))
							aken.blit(choose, (235,135))
							aken.blit(enter, (195,400))
							aken.blit(cave, (45,10))
							loendur = 0
							for nupp in nupud:
								aken.blit(nupud[loendur], (koordx[loendur],koordy[loendur]))
								loendur +=1
							aken.blit(spraidid[sprait], (260,225))
							aken.blit(box, (205,460))
							pygame.display.flip()
							loendur = 0
						else:
							return nimi, "./pildid/p"+str(sprait+1)+"_small.png", "./pildid/p"+str(sprait+1)+"_battle.png"
					elif loendur == 3:
						return start()
						
				else:
					aken.blit(nupud[loendur], (koordx[loendur],koordy[loendur]))
					aken.blit(spraidid[sprait], (260,225))
				loendur += 1
			
		elif kysimus == 2:
			aken.blit(box, (205,460))
			nimi = ask(vastus)
			joonista(aken ,nimi)

		elif kysimus == 3:
			for nupp in nupud:
				rnupp=getrect(nupp, loendur, koordx, koordy)
				if rnupp.collidepoint(pygame.mouse.get_pos()):
					aken.blit(nupudo[loendur], (koordx[loendur],koordy[loendur]))
				else:
					aken.blit(nupud[loendur], (koordx[loendur],koordy[loendur]))
					aken.blit(spraidid[sprait], (260,225))
				loendur += 1
		pygame.display.flip()
		event = pygame.event.poll()

def write(text,x,y,size,color, aken):
	font_obj = pygame.font.Font('./pildid/D3Stonism.ttf', size)
	text_surface_obj = font_obj.render(text, True, color)
	text_rect_obj = text_surface_obj.get_rect()
	text_rect_obj.center = (x,y)
	aken.blit(text_surface_obj, text_rect_obj)
	
def info():
	aken = pygame.display.set_mode((675,675))
	aken.fill( (192,192,192) )
	ekraan=[info1,info2,info3]
		
	nupud = [parem, vasak, back]
	koordx = [520, 120, 120]
	koordy = [340, 340, 160]
	infoloendur = 0
	aken.blit(cave, (45,10))
	event = pygame.event.poll()
	aken.blit(ekraan[infoloendur], (85,110))
	while True:
		loendur=0
		for nupp in nupud:
			rnupp=getrect(nupp, loendur, koordx, koordy)
			if rnupp.collidepoint(pygame.mouse.get_pos()):
				aken.blit(nupud[loendur], (koordx[loendur],koordy[loendur]))
				if event.type == pygame.MOUSEBUTTONDOWN:
					select[randint(1,5)-1].play()
					if loendur == 0:
						if infoloendur == 2:
							infoloendur = 0
							aken.blit(ekraan[infoloendur], (85,110))
						else:
							infoloendur += 1
							aken.blit(ekraan[infoloendur], (85,110))
					elif loendur ==  1:
						if infoloendur == 0:
							infoloendur = 2
							aken.blit(ekraan[infoloendur], (85,110))
						else:
							infoloendur -= 1
							aken.blit(ekraan[infoloendur], (85,110))
					elif loendur == 2:
						return start()
			else:
				aken.blit(nupud[loendur], (koordx[loendur],koordy[loendur]))
			loendur += 1
		pygame.display.flip()
		event = pygame.event.poll()

def highscores():
	aken = pygame.display.set_mode((675,675))
	aken.fill( (192,192,192) )
	aken.blit(taust, (85,110))
	aken.blit(cave, (45,10))
	aken.blit(skoorpealkiri, (195,160))
	nupud = [back]
	koordx = [120]
	koordy = [160]
	nimed, punktid = readscores()
	loendur=len(nimed)
	pygame.display.flip()
	ykoord=290
	while loendur != 0:
		punktid, nimed, punkt, nimi = gethighest(punktid, nimed)
		write(nimi+'    '+str(punkt), 340, ykoord, 30, (64,64,64), aken)
		loendur -=1
		ykoord += 70
		pygame.display.flip()
	while True:
		loendur=0
		for nupp in nupud:
			rnupp=getrect(nupp, loendur, koordx, koordy)
			if rnupp.collidepoint(pygame.mouse.get_pos()):
				aken.blit(nupud[loendur], (koordx[loendur],koordy[loendur]))
				if event.type == pygame.MOUSEBUTTONDOWN:
					return start()
			else:
				aken.blit(nupud[loendur], (koordx[loendur],koordy[loendur]))
			loendur += 1
		pygame.display.flip()
		event = pygame.event.poll()

